package wetalk.utils;

import wetalk.db.models.User;

/**
 * Contains methods to maintain the current logged user session
 */
public class UserSession {

    private static User currentUser; // Keeps the current logged user

    /**
     * We are going to use the singleton pattern, so let's keep this private
     */
    private UserSession() {
    }

    /**
     * @return the current logged in user
     * @exception RuntimeException if there is no logged user
     */
    public static User getCurrentUser() {
        if (currentUser == null) {
            throw new RuntimeException("No user logged in.");
        }
        return currentUser;
    }

    /**
     * @param user The user who is attemping login
     * @exception RuntimeException if there is already logged user
     */
    public static void login(User user) {
        if (currentUser != null) {
            throw new RuntimeException("Already logged in.");
        }
        currentUser = user;
    }

    /**
     * Removes the current logged user
     *
     * @exception RuntimeException if there is no logged user
     */
    public static void logout() {
        if (currentUser == null) {
            throw new RuntimeException("Already logged in.");
        }
        currentUser = null;
    }
}
