package wetalk.ui.searchuser;

import wetalk.db.models.User;
import wetalk.lists.circular.DoubleCircularList;
import wetalk.lists.simple.EndInsSimpleList;

/**
 * This specifies the contract between search user window and the sign in
 * presenter.
 */
public interface ISearchUserView {

    /**
     * Called when the presenter finishes to load all users.
     *
     * @param users list with all the users
     */
    void onGetAllUsersSuccess(DoubleCircularList<User> users);

    /**
     * Called when the presenter finishes to load all common friends.
     *
     * @param users all common friends list
     */
    void onGetAllCommonFriendsSuccess(EndInsSimpleList<User> users);

}
