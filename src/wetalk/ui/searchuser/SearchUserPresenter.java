package wetalk.ui.searchuser;

import wetalk.db.repository.FriendsRepository;
import wetalk.db.repository.UserRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI
 * ({@link wetalk.ui.searchuser.SearchUserFrame}), retrieves the data and
 * updates UI as required.
 */
public class SearchUserPresenter {

    private ISearchUserView view; // Allows communication with the windows

    /**
     * Creates a preseter for a ({@link wetalk.ui.searchuser.SearchUserFrame}).
     *
     * @param view interface for communication with the window
     */
    public SearchUserPresenter(ISearchUserView view) {
        this.view = view;
    }

    public void getAllUsers() {
        view.onGetAllUsersSuccess(UserRepository.getInstance().getUserList());
    }

    public void getCommonFriends() {
        view.onGetAllCommonFriendsSuccess(FriendsRepository.getInstance()
                .getCommonFriends(UserSession.getCurrentUser()));
    }

}
