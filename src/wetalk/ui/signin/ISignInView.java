package wetalk.ui.signin;

/**
 * This specifies the contract between sign in window and the sign in presenter.
 */
public interface ISignInView {

    /**
     * Called when the register operation is completed successfully.
     */
    void onSignInSuccess();

    /**
     * Called if there is alaready an user with the same user name.
     */
    void userAlreadyExists();

    /**
     * Called if there is alaready an user with the same id.
     */
    void idAlreadyExists();

}
