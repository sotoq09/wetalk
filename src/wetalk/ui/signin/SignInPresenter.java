package wetalk.ui.signin;

import wetalk.db.models.User;
import wetalk.db.repository.UserRepository;

/**
 * Listen user actions from the UI ({@link wetalk.ui.signin.SignInWindow}),
 * retrieves the data and updates UI as required.
 */
public class SignInPresenter {

    private ISignInView view; // Allows communication with the windows

    /**
     * Creates a preseter for a ({@link wetalk.ui.signin.SignInWindow}).
     *
     * @param view interface for communication with the window
     */
    public SignInPresenter(ISignInView view) {
        this.view = view;
    }

    /**
     * Attemps to register an user with the given data.
     *
     * @param imagePath
     * @param userName
     * @param password
     * @param id
     * @param name
     * @param lastName
     * @param city
     * @param country
     */
    public void executeSignIn(String imagePath, String userName,
            String password, int id, String name, String lastName,
            String city, String country) {

        // Validations
        if (UserRepository.getInstance().userIdExists(id)) {
            view.idAlreadyExists();
        } else if (UserRepository.getInstance().userNameExists(userName)) {
            view.userAlreadyExists();
        } else {
            // Try to register
            User user = new User(userName, password, country, city, id, name,
                    lastName, User.UserType.REGULAR);
            user.setPhotoUri(imagePath);
            UserRepository.getInstance().addUser(user);
            view.onSignInSuccess();
        }
    }

}
