package wetalk.ui;

public interface RefreshListener {
    
    /**
     * Called when we need to refresh the data from a internal frame.
     */
    void onRefresh();
    
}
