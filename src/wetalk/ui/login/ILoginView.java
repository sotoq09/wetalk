package wetalk.ui.login;

import wetalk.db.models.User;

/**
 * This specifies the contract between login window and the login presenter.
 */
public interface ILoginView {

    /**
     * This method will be called when the user log in successfully.
     *
     * @param loggedUser the user that logged in
     */
    void onLoginSuccess(User loggedUser);

    /**
     * Called if the user doesn't exists or if the password is wrong.
     */
    void onInvalidCredentials();

    /**
     * Called when the presenter finish to save the data in files. It's used to
     * know when we can close the window.
     */
    void onSaveDataFinished();

}
