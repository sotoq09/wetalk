package wetalk.ui.login;

import wetalk.db.models.User;
import wetalk.db.repository.DataLoader;
import wetalk.db.repository.UserRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.login.LoginWindow}),
 * retrieves the data and updates UI as required.
 */
public class LoginPresenter {

    private ILoginView view; // Allows communication with the windows

    /**
     * Creates a presenter for a ({@link wetalk.ui.login.LoginWindow}).
     *
     * @param view interface for communication with the window
     */
    public LoginPresenter(ILoginView view) {
        this.view = view;
    }

    /**
     * Attempts to log in with the given data.
     * 
     * @param userName user name
     * @param password user password
     */
    public void executeLogin(String userName, String password) {
        // Loops trough the user list
        for (User user : UserRepository.getInstance().getUserList()) {
            // Check the current user with the given data
            if (userName.equalsIgnoreCase(user.getUsername())
                    && password.equalsIgnoreCase(user.getPassword())) {

                UserSession.login(user);    // Saves logged user
                view.onLoginSuccess(user);  // Success login callback
                return;
            }
        }

        view.onInvalidCredentials();  // Incorrect login callback
    }

    /**
     * Saves the data in files.
     */
    public void saveData() {
        DataLoader.saveData();
        view.onSaveDataFinished();
    }
}
