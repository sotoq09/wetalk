package wetalk.ui.friends;

import wetalk.db.models.FriendList;
import wetalk.db.models.User;
import wetalk.lists.List;
import wetalk.lists.circular.EndInsCircularList;

/**
 * This specifies the contract between search user window and the sign in
 * presenter.
 */
public interface IFriendsView {

    /**
     * Called when the presenter finishes to load all the friends lists from the
     * logged user.
     *
     * @param friendLists list with all the friendlists
     */
    void onGetAllFriendLists(EndInsCircularList<FriendList> friendLists);

    /**
     * Called when the presenter finishes to removes the friend list.
     */
    void onFriendListDeleteSuccess();

    /**
     * Called if the can't remove the friend list.
     */
    void onFriendListDeleteError();

    /**
     * Called when the presenter finishes to removes the friend.
     */
    void onFriendDeleteSuccess();

    /**
     * Called if the can't remove the friend.
     */
    void onFriendDeleteError();

    /**
     * Response for user search in friend lists.
     *
     * @param results search results
     */
    void onSearchFriendResults(User friend, List<FriendList> results);

}
