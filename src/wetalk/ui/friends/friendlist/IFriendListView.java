package wetalk.ui.friends.friendlist;

/**
 * This specifies the contract between search user window and the sign in
 * presenter.
 */
public interface IFriendListView {

    /**
     * Called when the presenter finishes to create a new friendList.
     */
    void onAddFriendListsSuccess();

    /**
     * Called if the presenter can't add the friend list.
     */
    void onAddFriendListsError();

    /**
     * Called when the presenter finishes to modify friendList.
     */
    void onModifyFriendListsSuccess();

    /**
     * Called if the presenter can't modify the friend list.
     */
    void onModifyFriendListsError();

}
