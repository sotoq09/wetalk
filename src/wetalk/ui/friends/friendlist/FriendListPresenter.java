package wetalk.ui.friends.friendlist;

import wetalk.db.models.FriendList;
import wetalk.db.repository.FriendsRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI
 * ({@link wetalk.ui.friends.friendlist.FriendListFrame}), retrieves the data
 * and updates UI as required.
 */
public class FriendListPresenter {

    private IFriendListView view; // Allows communication with the windows

    /**
     * Creates a presenter for a
     * ({@link wetalk.ui.friends.friendlist.FriendListFrame}).
     *
     * @param view interface for communication with the window
     */
    public FriendListPresenter(IFriendListView view) {
        this.view = view;
    }

    /**
     * Creates a new friend list and adds it to current user.
     *
     * @param name name of the list
     * @param description description of the list
     */
    public void addFriendList(String name, String description) {

        boolean success = FriendsRepository.getInstance().addFriendList(
                UserSession.getCurrentUser(), name, description);

        if (success) {
            view.onAddFriendListsSuccess();
        } else {
            view.onAddFriendListsError();
        }
    }

    /**
     * Modifies an existing friendList from the current user.
     *
     * @param friendList friendList
     * @param newName new name
     * @param newDescription new description
     */
    public void modifyFriendList(FriendList friendList, String newName,
            String newDescription) {

        boolean success = FriendsRepository.getInstance().modifyFriendList(
                UserSession.getCurrentUser(), friendList, newName,
                newDescription);

        if (success) {
            view.onModifyFriendListsSuccess();
        } else {
            view.onModifyFriendListsError();
        }
    }

    /**
     * Checks if the specified friend lists exists in the current user.
     * 
     * @param listName name of the list
     * @return true if the lists exists
     */
    public boolean friendListExists(String listName) {
        return FriendsRepository.getInstance().
                getFriendList(UserSession.getCurrentUser(), listName) != null;
    }

}
