package wetalk.ui.friends;

import wetalk.db.models.FriendList;
import wetalk.db.models.User;
import wetalk.db.repository.FriendsRepository;
import wetalk.db.repository.UserRepository;
import wetalk.lists.List;
import wetalk.lists.simple.EndInsSimpleList;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.friends.FriendsFrame}),
 * retrieves the data and updates UI as required.
 */
public class FriendsPresenter {

    private IFriendsView view; // Allows communication with the windows

    /**
     * Creates a presenter for a ({@link wetalk.ui.friends.FriendsFrame}).
     *
     * @param view interface for communication with the window
     */
    public FriendsPresenter(IFriendsView view) {
        this.view = view;
    }

    /**
     * Retrieves all the friends lists from the logged user
     */
    public void getAllFriendsLists() {
        view.onGetAllFriendLists(FriendsRepository.getInstance()
                .getAllFriendLists(UserSession.getCurrentUser()));
    }

    /**
     * Deletes the specified friend list from the current user.
     *
     * @param friendList list to delete
     */
    public void deleteFriendList(FriendList friendList) {
        if (FriendsRepository.getInstance().removeFriendList(
                UserSession.getCurrentUser(), friendList.getListName())) {
            view.onFriendListDeleteSuccess();
        } else {
            view.onFriendListDeleteError();
        }
    }

    /**
     * Deletes a friend from the specified friend list.
     *
     * @param friend
     * @param friendList
     */
    public void deleteFriend(User friend, FriendList friendList) {
        if (FriendsRepository.getInstance().removeFriend(friendList, friend)) {
            view.onFriendDeleteSuccess();
        } else {
            view.onFriendDeleteError();
        }
    }

    /**
     * Search all the lists with contains the specified user.
     *
     * @param friendName user name
     */
    public void searchFriendInList(String friendName) {
        List<FriendList> results = new EndInsSimpleList<>();

        // Let's get the user using it's name
        User friend = UserRepository.getInstance().getUserList().
                searchElement((fr) -> {
                    return fr.getUsername().equalsIgnoreCase(friendName);
                });

        // Try to search for data only if we can get the user
        if (friend != null) {
            results = FriendsRepository.getInstance().searchFriendInLists(
                    UserSession.getCurrentUser(), friend);
        }

        view.onSearchFriendResults(friend, results);
    }

}
