package wetalk.ui.reports;

import wetalk.db.models.Report.ReportStatus;
import wetalk.db.repository.ReportRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI
 * ({@link wetalk.ui.reports.SearchReportsFrame}), retrieves the data and
 * updates UI as required.
 */
public class SearchReportPresenter {

    private ISearchReportView view; // Allows communication with the windows

    /**
     * Creates a presenter for a ({@link wetalk.ui.reports.SearchReportsFrame}).
     *
     * @param view interface for communication with the window
     */
    public SearchReportPresenter(ISearchReportView view) {
        this.view = view;
    }

    /**
     * Loads reports by status.
     *
     * @param filter status to filter
     */
    public void loadReports(ReportStatus filter) {
        view.onGetReports(ReportRepository.getInstance().getReports(filter));
    }

    /**
     * Loads reports made by the current user. Filters by status.
     *
     * @param filter status to filter
     */
    public void loadMadeReports(ReportStatus filter) {
        view.onGetReports(ReportRepository.getInstance().getReportsByDenouncer(
                UserSession.getCurrentUser(), filter));
    }

    /**
     * Loads reports that the current user received. Filter by status.
     *
     * @param filter status to filter
     */
    public void loadReceivedReports(ReportStatus filter) {
        view.onGetReports(ReportRepository.getInstance().getReportsByReported(
                UserSession.getCurrentUser(), filter));
    }
}
