package wetalk.ui.reports;

import wetalk.db.models.Report;
import wetalk.lists.simple.EndInsSimpleList;

/**
 * This specifies the contract between search report window and the search
 * report presenter.
 */
public interface ISearchReportView {

    /**
     * Called when the presenter finishes to load all the reports.
     *
     * @param reports list with the reports
     */
    void onGetReports(EndInsSimpleList<Report> reports);

}
