package wetalk.ui.reports.newreport;

/**
 * This specifies the contract between password window and the password
 * presenter.
 */
public interface INewReportView {

    /**
     * Called if the report was created successfully.
     */
    void onCreateReportSuccess();

    /**
     * Called if the report was accepted successfully.
     */
    void onReportAcceptSuccess();

    /**
     * Called if the report was rejected successfully.
     */
    void onReportRejectSuccess();

    /**
     * Called if the report can't be accepted.
     */
    void onReportAcceptError();

    /**
     * Called if the report can't be accepted.
     */
    void onReportRejectError();

}
