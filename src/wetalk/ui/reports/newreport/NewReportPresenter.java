package wetalk.ui.reports.newreport;

import wetalk.db.models.Report;
import wetalk.db.models.User;
import wetalk.db.repository.ReportRepository;
import wetalk.db.repository.UserRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI
 * ({@link wetalk.ui.reports.newreport.NewReportFrame}), retrieves the data and
 * updates UI as required.
 */
public class NewReportPresenter {

    private INewReportView view; // Allows communication with the windows

    /**
     * Creates a presenter for a
     * ({@link wetalk.ui.reports.newreport.NewReportFrame}).
     *
     * @param view interface for communication with the window
     */
    public NewReportPresenter(INewReportView view) {
        this.view = view;
    }

    /**
     * Creates a new report with the given data.
     *
     * @param title title of the report
     * @param clientMsg message of the client
     * @param denouncedUser user that was denounced
     */
    public void newReport(String title, String clientMsg, User denouncedUser) {
        ReportRepository.getInstance().addReport(title, clientMsg, "",
                Report.ReportStatus.PENDING, UserSession.getCurrentUser(),
                denouncedUser);
        view.onCreateReportSuccess();
    }

    /**
     * Accepts the given report and bans the denounced user.
     *
     * @param report report
     * @param adminResponse message with the response of the admin
     */
    public void acceptReport(Report report, String adminResponse) {
        if (ReportRepository.getInstance().acceptReport(report, adminResponse)) {
            UserRepository.getInstance().removeUser(report.getReportedUser());
            view.onReportAcceptSuccess();
        } else {
            view.onReportAcceptError();
        }
    }

    /**
     * Rejects the given report.
     *
     * @param report report
     * @param adminResponse message with the response of the admin
     */
    public void rejectReport(Report report, String adminResponse) {
        if (ReportRepository.getInstance().rejectReport(report, adminResponse)) {
            view.onReportRejectSuccess();
        } else {
            view.onReportRejectError();
        }
    }
}
