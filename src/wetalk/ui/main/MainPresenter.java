package wetalk.ui.main;

import wetalk.db.repository.DataLoader;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.main.MainWindow}),
 * retrieves the data and updates UI as required.
 */
public class MainPresenter {

    private IMainView view; // Allows communication with the windows

    /**
     * Creates a preseter for a ({@link wetalk.ui.login.LoginWindow}).
     *
     * @param view interface for communication with the window
     */
    public MainPresenter(IMainView view) {
        this.view = view;
    }

    /**
     * Saves the data in files.
     */
    public void saveData() {
        DataLoader.saveData();
        view.onSaveDataFinished();
    }

    /**
     * Log out from the app
     */
    public void logout() {
        UserSession.logout();
        view.onLogoutSuccess();
    }
}
