package wetalk.ui.main;

/**
 * This specifies the contract between login window and the login presenter.
 */
public interface IMainView {

    /**
     * Called when the presenter finishes log out operations.
     */
    void onLogoutSuccess();

    /**
     * Called when the presenter finish to save the data in files. It's used to
     * know when we can close the window.
     */
    void onSaveDataFinished();
}
