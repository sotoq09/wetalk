package wetalk.ui.user;

import wetalk.db.models.FriendList;
import wetalk.db.models.User;
import wetalk.db.repository.FriendsRepository;
import wetalk.db.repository.UserRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.user.UserFrame}), retrieves
 * the data and updates UI as required.
 */
public class UserPresenter {

    private IUserView view; // Allows communication with the windows

    /**
     * Creates a preseter for a ({@link wetalk.ui.signin.SignInWindow}).
     *
     * @param view interface for communication with the window
     */
    public UserPresenter(IUserView view) {
        this.view = view;
    }

    /**
     * Removes the desired user.
     *
     * @param user user to delete
     */
    public void removeUser(User user) {
        UserRepository.getInstance().removeUser(user);
        UserSession.logout();
        view.onRemoveUserSuccess(); // Success callback
    }

    /**
     * Attempts to modify an user.
     *
     * @param user User to modify
     * @param imageUri new image path
     * @param username new username
     * @param name new name
     * @param lastName new last name
     * @param city new city
     * @param country new country
     * @param id new id
     */
    public void modifyUser(User user, String imageUri, String username,
            String name, String lastName, String city, String country,
            int id) {

        if (UserRepository.getInstance().modifyUser(user, imageUri, username,
                name, lastName, city, country, id)) {
            view.onModifySuccess(); // Success callback
        } else {
            view.userAlreadyExistsError(); // Error callback            
        }
    }

    /**
     * Attempts to add a new friend to the specified list.
     *
     * @param friend friend to add
     * @param friendList list
     */
    public void addFriend(User friend, FriendList friendList) {
        if (FriendsRepository.getInstance().addFriend(friend, friendList)) {
            view.onFriendAddSuccess();
        } else {
            view.onFriendAddError();
        }
    }

}
