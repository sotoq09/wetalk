package wetalk.ui.user;

/**
 * This specifies the contract between sign in window and the sign in presenter.
 */
public interface IUserView {

    /**
     * Called when the user is removed successfully.
     */
    void onRemoveUserSuccess();

    /**
     * Called when the user is modified successfully.
     */
    void onModifySuccess();

    /**
     * Called if the new name already exists.
     */
    void userAlreadyExistsError();

    /**
     * Called when a new friend is added successfully.
     */
    void onFriendAddSuccess();

    /**
     * Called if the friend can't be added.
     */
    void onFriendAddError();

}
