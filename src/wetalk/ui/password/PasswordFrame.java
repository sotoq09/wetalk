package wetalk.ui.password;

import javax.swing.JOptionPane;
import wetalk.ui.RefreshListener;

public class PasswordFrame extends javax.swing.JInternalFrame
        implements IPasswordView, RefreshListener {

    private PasswordPresenter presenter;

    /**
     * Creates new form PasswordFrame
     */
    public PasswordFrame() {
        presenter = new PasswordPresenter(this);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblActualPw = new javax.swing.JLabel();
        txtActualPw = new javax.swing.JPasswordField();
        lblNewPw = new javax.swing.JLabel();
        txtNewPw = new javax.swing.JPasswordField();
        lblConfirmPw = new javax.swing.JLabel();
        txtConfirmPw = new javax.swing.JPasswordField();
        btnChange = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setClosable(true);
        setTitle("Change password");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/wetalk/ui/resources/key.png"))); // NOI18N

        lblActualPw.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        lblActualPw.setIcon(new javax.swing.ImageIcon(getClass().getResource("/wetalk/ui/resources/key.png"))); // NOI18N
        lblActualPw.setText("Actual password");
        lblActualPw.setToolTipText("");

        lblNewPw.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        lblNewPw.setIcon(new javax.swing.ImageIcon(getClass().getResource("/wetalk/ui/resources/key.png"))); // NOI18N
        lblNewPw.setText("New password");
        lblNewPw.setToolTipText("");

        lblConfirmPw.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        lblConfirmPw.setIcon(new javax.swing.ImageIcon(getClass().getResource("/wetalk/ui/resources/key.png"))); // NOI18N
        lblConfirmPw.setText("Confirm password");
        lblConfirmPw.setToolTipText("");

        txtConfirmPw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtConfirmPwActionPerformed(evt);
            }
        });

        btnChange.setIcon(new javax.swing.ImageIcon(getClass().getResource("/wetalk/ui/resources/shuffle.png"))); // NOI18N
        btnChange.setText("Change");
        btnChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeActionPerformed(evt);
            }
        });

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/wetalk/ui/resources/cancel (1).png"))); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtActualPw)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblActualPw)
                                    .addComponent(lblNewPw)
                                    .addComponent(lblConfirmPw))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtNewPw)
                            .addComponent(txtConfirmPw))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 31, Short.MAX_VALUE)
                        .addComponent(btnChange)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancel)
                        .addGap(39, 39, 39))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblActualPw)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtActualPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblNewPw)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNewPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblConfirmPw)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtConfirmPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnChange)
                    .addComponent(btnCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeActionPerformed
        changePassword(String.copyValueOf(txtActualPw.getPassword()),
                String.copyValueOf(txtNewPw.getPassword()),
                String.copyValueOf(txtConfirmPw.getPassword()));
    }//GEN-LAST:event_btnChangeActionPerformed

    private void txtConfirmPwActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtConfirmPwActionPerformed
        changePassword(String.copyValueOf(txtActualPw.getPassword()),
                String.copyValueOf(txtNewPw.getPassword()),
                String.copyValueOf(txtConfirmPw.getPassword()));
    }//GEN-LAST:event_txtConfirmPwActionPerformed

    private void changePassword(String originalPw, String newPw,
            String newPwConfirm) {

        if (newPw.length() < 6) {
            JOptionPane.showMessageDialog(this, "Password must have 6 characters.",
                    "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (newPw.equals(newPwConfirm)) {
            presenter.changePassword(originalPw, newPw);
        } else {
            JOptionPane.showInternalMessageDialog(this,
                    "Passwords are different.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void onChangePasswordSuccess() {
        JOptionPane.showInternalMessageDialog(this,
                "Password change successfully.",
                "Successfull operation",
                JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void onChangePasswordError() {
        JOptionPane.showInternalMessageDialog(this,
                "Current password is incorrect.",
                "Error",
                JOptionPane.ERROR_MESSAGE);
    }
    
    @Override
    public void onRefresh() {
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnChange;
    private javax.swing.JLabel lblActualPw;
    private javax.swing.JLabel lblConfirmPw;
    private javax.swing.JLabel lblNewPw;
    private javax.swing.JPasswordField txtActualPw;
    private javax.swing.JPasswordField txtConfirmPw;
    private javax.swing.JPasswordField txtNewPw;
    // End of variables declaration//GEN-END:variables

}
