package wetalk.ui.password;

import wetalk.db.models.User;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.password.PasswordFrame}),
 * retrieves the data and updates UI as required.
 */
public class PasswordPresenter {
    
    private IPasswordView view; // Allows communication with the windows

    /**
     * Creates a preseter for a ({@link wetalk.ui.login.LoginWindow}).
     *
     * @param view interface for communication with the window
     */
    public PasswordPresenter(IPasswordView view) {
        this.view = view;
    }

    /**
     * Atempts to change the password of the current logged user.
     *
     * @param currentPassword the cuurrent password of the user
     * @param newPassword the new password
     */
    public void changePassword(String currentPassword, String newPassword) {
        User user = UserSession.getCurrentUser();
        if (user.getPassword().equals(currentPassword)) {
            user.setPassword(newPassword);
            view.onChangePasswordSuccess();
        } else {
            view.onChangePasswordError();
        }
    }
}
