package wetalk.ui.password;

/**
 * This specifies the contract between password window and the password
 * presenter.
 */
public interface IPasswordView {

    /**
     * Called when the presenter finishes change password opearation
     * successfully.
     */
    void onChangePasswordSuccess();

    /**
     * Called when the presenter finish the password change with errors.
     */
    void onChangePasswordError();
}
