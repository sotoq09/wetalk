package wetalk.ui;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 * A custom desktop pane with a method that allows to refresh the internal
 * frames.
 */
public class RefreshableDesktopPane extends JDesktopPane {

    /**
     * Gets all the frames in the desktop pane and refresh each one.
     *
     * @throws IllegalStateException if one frame doesn't implements
     * {@link RefreshListener}
     */
    public void refreshFrames() {
        for (JInternalFrame frame : getAllFrames()) {
            if (frame instanceof RefreshListener) {
                ((RefreshListener) frame).onRefresh();
            } else {
                throw new IllegalStateException(
                        "All frames must implement Refresh Listener");
            }
        }
    }
}
