package wetalk.ui.messages;

import wetalk.db.models.Message;
import wetalk.lists.List;

/**
 * This specifies the contract between search user window and the sign in
 * presenter.
 */
public interface IMessagesView {

    /**
     * Called if the message was sent successfully.
     */
    void onMessageSentSuccess();

    /**
     * Called if a destinatary doesn't exists.
     */
    void onMessageBadDestintaries();

    /**
     * Called if the message was modified successfully.
     */
    void onMessageModifySuccess();

    /**
     * Called if the message can't be modified.
     */
    void onMessageModifyError();

    /**
     * Called when the presenter finishes to load all the sent messages.
     *
     * @param results list with messages
     */
    void onLoadSentMessages(List<Message> results);

    /**
     * Called when the presenter finishes to load all the received messages.
     *
     * @param results list with messages
     */
    void onLoadReceivedMessages(List<Message> results);

    /**
     * Called if the message was deleted successfully.
     */
    void onMessageDeleteSuccess();

    /**
     * Called if there was an error deleting the message.
     */
    void onMessageDeleteError();

}
