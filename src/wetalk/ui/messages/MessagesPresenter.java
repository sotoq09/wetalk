package wetalk.ui.messages;

import wetalk.db.models.FriendList;
import wetalk.db.models.Message;
import wetalk.db.models.User;
import wetalk.db.repository.FriendsRepository;
import wetalk.db.repository.MessageRepository;
import wetalk.lists.simple.EndInsSimpleList;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.messages.MessagesFrame}),
 * retrieves the data and updates UI as required.
 */
public class MessagesPresenter {

    private IMessagesView view; // Allows communication with the windows

    /**
     * Creates a preseter for a ({@link wetalk.ui.messages.MessagesFrame}).
     *
     * @param view interface for communication with the window
     */
    public MessagesPresenter(IMessagesView view) {
        this.view = view;
    }

    /**
     * Sends a messages to the specified destinataries.
     *
     * @param strDestinataries destinataries
     * @param message message text
     * @param imagePath image
     */
    public void sentMessage(String strDestinataries, String message,
            String imagePath) {

        String[] friendListsNames = strDestinataries.split(";");
        EndInsSimpleList<User> destinataries = new EndInsSimpleList<>();

        for (String listName : friendListsNames) {
            FriendList friendList = FriendsRepository.getInstance()
                    .getFriendList(UserSession.getCurrentUser(), listName.trim());

            if (friendList == null) {
                view.onMessageBadDestintaries();
                return;
            }

            for (User friend : friendList.getFriends()) {
                if (!destinataries.contains(friend)) {
                    destinataries.add(friend);
                }
            }
        }

        MessageRepository.getInstance().sentMessage(UserSession.getCurrentUser(),
                message, imagePath, destinataries);
        view.onMessageSentSuccess();
    }

    /**
     * Retrieves all sent messages from the current user.
     */
    public void loadSentMessages() {
        view.onLoadSentMessages(MessageRepository.getInstance()
                .getSentMessages(UserSession.getCurrentUser()));
    }

    /**
     * Retrieves all received messages from the current user.
     */
    public void loadReceivedMessages() {
        view.onLoadReceivedMessages(MessageRepository.getInstance()
                .getReceivedMessages(UserSession.getCurrentUser()));
    }
    
    public void modifyMessage(Message message, String text, 
            String imagePath) {
        if (MessageRepository.getInstance().modifySentMessage(
                UserSession.getCurrentUser(), message, text, imagePath)) {
            view.onMessageModifySuccess();
        } else {
            view.onMessageModifyError();
        }
    }

    public void removeMessage(Message message) {
        if (MessageRepository.getInstance().removeSentMessage(
                UserSession.getCurrentUser(), message)) {
            view.onMessageDeleteSuccess();
        } else {
            view.onMessageDeleteError();
        }
    }

}
