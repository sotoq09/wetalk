package wetalk.ui.events;

import wetalk.db.models.Event;
import wetalk.db.repository.EventRepository;
import wetalk.utils.UserSession;

/**
 * Listen user actions from the UI ({@link wetalk.ui.events.EventsFrame}),
 * retrieves the data and updates UI as required.
 */
public class EventsPresenter {

    private IEventsView view;

    /**
     * Creates a presenter for a ({@link wetalk.ui.events.EventsFrame}).
     *
     * @param view interface for communication with the window
     */
    public EventsPresenter(IEventsView view) {
        this.view = view;
    }

    /**
     * Retrieves all the events. Optional filter by status.
     *
     * @param status status to filter. Can be null if we don't want to use it
     */
    public void getAllEvents(Event.EventStatus status) {
        if (status == null) {
            view.onGetEvents(EventRepository.getInstance().getEventsList());
        } else {
            view.onGetEvents(EventRepository.getInstance()
                    .getEventsByFilter(status));
        }
    }

    /**
     * Get all the events created by the logged user. Optional filter by status.
     *
     * @param status status to filter. Can be null if we don't want to use it
     */
    public void getUserCreatedEvents(Event.EventStatus status) {
        view.onGetEvents(EventRepository.getInstance()
                .getUserCreatedEvents(UserSession.getCurrentUser(), status));
    }

    /**
     * Get all the events in which the logged user is invited. Optional filter
     * by status.
     *
     * @param status status to filter. Can be null if we don't want to use it
     */
    public void getUserInvitedEvents(Event.EventStatus status) {
        view.onGetEvents(EventRepository.getInstance()
                .getUserInvitedEvents(UserSession.getCurrentUser(), status));
    }

    /**
     * Removes the specified event.
     *
     * @param event event to remove
     */
    public void deleteEvent(Event event) {
        if (EventRepository.getInstance().removeEvent(event)) {
            view.onDeleteEventSuccess();
        } else {
            view.onDeleteEventError();
        }
    }

}
