package wetalk.ui.events.eventsList;

import java.util.Date;
import wetalk.db.models.Event;
import wetalk.db.models.FriendList;
import wetalk.db.repository.EventRepository;
import wetalk.db.repository.FriendsRepository;
import wetalk.lists.circular.EndInsCircularList;
import wetalk.utils.UserSession;

/*
 * Listen user actions from the UI
 * ({@link wetalk.ui.events.eventList.EventListFrame}), retrieves the data
 * and updates UI as required.
 */
public class NewEventPresenter {

    private INewEventView view; // Allows communication with the windows

    /**
     * Creates a presenter for
     * ({@link wetalk.ui.events.eventsList.EventListFrame})
     *
     * @param view interface for communication with the window
     */
    public NewEventPresenter(INewEventView view) {
        this.view = view;
    }

    /*
    * Creates a new event and adds it to current user.
     *
     * @param name name of the event
     * @param description description of the event
     * @param city of the event
     * @param date of the event
     * @param status of the event
     * @param strFriendLists names of the friendlists
     */
    public void addEvent(String name, String description, String city,
            Date date, String strFriendLists) {

        String[] friendListsNames = strFriendLists.split(";");
        EndInsCircularList<FriendList> friendLists = new EndInsCircularList<>();

        for (String listName : friendListsNames) {
            FriendList friendList = FriendsRepository.getInstance()
                    .getFriendList(UserSession.getCurrentUser(), listName.trim());

            if (friendList == null) {
                view.onFriendListNotExistsError();
                return;
            }
            friendLists.add(friendList);
        }

        EventRepository.getInstance().createEvent(name, description, city, date,
                Event.EventStatus.PENDING, UserSession.getCurrentUser(),
                friendLists);

        view.onAddEventSuccess();
    }

    /**
     * Modifies an existing event from the current user.
     *
     * @param event
     * @param name of the event.
     * @param description of the event.
     * @param city of the event.
     * @param date of the event.
     * @param status of the event.
     * @param strFriendLists name of the friend lists.
     */
    public void modifyEvent(Event event, String name, String description,
            String city, Date date, Event.EventStatus status,
            String strFriendLists) {

        String[] friendListsNames = strFriendLists.split(";");
        EndInsCircularList<FriendList> friendLists = new EndInsCircularList<>();

        for (String listName : friendListsNames) {
            FriendList friendList = FriendsRepository.getInstance()
                    .getFriendList(UserSession.getCurrentUser(), listName.trim());

            if (friendList == null) {
                view.onFriendListNotExistsError();
                return;
            }
            friendLists.add(friendList);
        }

        if (EventRepository.getInstance().modifyEvent(event, name, description,
                city, date, status, friendLists)) {
            view.onModifyEventSuccess();
        } else {
            view.onModifyEventError();
        }
    }

}
