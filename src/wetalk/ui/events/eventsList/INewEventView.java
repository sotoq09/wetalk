package wetalk.ui.events.eventsList;

/**
 * This specifies the contract between search user window and the sign in
 * presenter.
 */
public interface INewEventView {

    /**
     * Called when the presenter finishes to create a new event .
     */
    void onAddEventSuccess();

    /**
     * Called when the presenter finishes to modify event .
     */
    void onModifyEventSuccess();

    /**
     * Called if the presenter can't modify the event .
     */
    void onModifyEventError();

    /**
     * Called if a friend list specified for event doesn't exists.
     */
    void onFriendListNotExistsError();

}
