package wetalk.ui.events;

import wetalk.db.models.Event;
import wetalk.lists.List;

/**
 * This specifies the contract between events window and the events presenter.
 */
public interface IEventsView {

    /**
     * Called when the presenter finishes to load the events.
     *
     * @param events events loaded
     */
    void onGetEvents(List<Event> events);

    /**
     * Called when the presenter finishes to removes the event.
     */
    void onDeleteEventSuccess();

    /**
     * Called when the presenter can't remove the event.
     */
    void onDeleteEventError();

}
