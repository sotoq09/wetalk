package wetalk.db.models;

import java.io.Serializable;
import java.util.Date;
import wetalk.lists.circular.EndInsCircularList;

public class Event implements Comparable<Event>, Serializable {

    // Event attributes
    private String id;
    private String name;
    private String description;
    private String city;
    private Date date;
    private EventStatus status;

    private User author;  // User who made the event

    // Contains all the friend that can see the event
    private EndInsCircularList<FriendList> friendVisibility;

    /**
     * Creates a new event object.
     *
     * @param id event id
     * @param name event name
     * @param description a brief description
     * @param city where the event takes place
     * @param date date of the event
     * @param status event status {@link EventStatus}
     * @param author user who created the event
     * @param friendVisibility friends that can see the event
     */
    public Event(String id, String name, String description, String city,
            Date date, EventStatus status, User author,
            EndInsCircularList<FriendList> friendVisibility) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.city = city;
        this.date = date;
        this.status = status;
        this.author = author;
        this.friendVisibility = friendVisibility;
    }

    // Getters and setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Returns the event status. If the event already finished, returns finished
     * status.
     *
     * @return the event status
     */
    public EventStatus getStatus() {
        Date currentDate = new Date();

        if (date.compareTo(currentDate) <= 0) {
            return EventStatus.FINISHED;
        }

        return status;
    }

    /**
     * Sets the status.
     *
     * @param status status
     * @throws IllegalArgumentException if the event finished.
     */
    public void setStatus(EventStatus status) {
        if (getStatus() == EventStatus.FINISHED) {
            throw new IllegalArgumentException("Event is finished!");
        }
        this.status = status;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public EndInsCircularList<FriendList> getFriendVisibility() {
        return friendVisibility;
    }

    public void setFriendVisibility(EndInsCircularList<FriendList> friendVisibility) {
        this.friendVisibility = friendVisibility;
    }

    /**
     * Compares this event with the specified event using the date.
     *
     * @param o the object to be compared
     * @return a negative integer, zero, or a positive integer as this object is
     * less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(Event o) {
        return date.compareTo(o.date);
    }

    /**
     * Constants that represents the status of the event.
     */
    public enum EventStatus {
        PENDING,
        CANCELLED,
        FINISHED
    }
}
