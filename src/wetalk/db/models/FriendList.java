/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wetalk.db.models;

import java.io.Serializable;
import wetalk.lists.simple.EndInsSimpleList;

/**
 * List of friends.
 */
public class FriendList implements Serializable {

    private String listName;
    private String listDescription;
    private EndInsSimpleList<User> friends;

    /**
     * Creates a new list of friends.
     *
     * @param listName name of the list
     * @param listDescription description of the list
     */
    public FriendList(String listName, String listDescription) {
        this.listName = listName;
        this.listDescription = listDescription;
        this.friends = new EndInsSimpleList<>();
    }

    // Getters and setters
    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListDescription() {
        return listDescription;
    }

    public void setListDescription(String listDescription) {
        this.listDescription = listDescription;
    }

    /**
     * Adds a new friend to this list.
     *
     * @param user the friend to add
     * @return false if he friend is already in the list
     */
    public boolean addFriend(User user) {
        if (friends.contains(user)) {
            return false;
        }

        friends.add(user);
        return true;
    }

    /**
     * Removes a friend from this list.
     *
     * @param user friend to remove
     * @return false if the friend isn't in the list
     */
    public boolean removeFriend(User user) {
        return friends.remove(user);
    }

    /**
     * Check if a friend is contained in this list.
     *
     * @param user friend to check
     * @return false if the friend isn't in the list
     */
    public boolean contains(User user) {
        return friends.contains(user);
    }

    public EndInsSimpleList<User> getFriends() {
        return friends;
    }

}
