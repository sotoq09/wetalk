package wetalk.db.models;

import java.io.Serializable;
import wetalk.lists.simple.EndInsSimpleList;

public class Message implements Serializable {

    // Message's attributes 
    private String id;
    private String text;
    private String imageUri;

    private User remitent;
    private EndInsSimpleList<User> destinataries;

    /**
     * Creates a new message
     *
     * @param id message id
     * @param text message text
     * @param imageUri path of the message's image
     * @param remitent user who sent the message
     * @param destinataries lst of destinataries
     */
    public Message(String id, String text, String imageUri, User remitent,
            EndInsSimpleList<User> destinataries) {
        this.id = id;
        this.text = text;
        this.imageUri = imageUri;
        this.remitent = remitent;
        this.destinataries = destinataries;
    }

    //Message's setters and getters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public User getRemitent() {
        return remitent;
    }

    public void setRemitent(User remitent) {
        this.remitent = remitent;
    }

    public EndInsSimpleList<User> getDestinataries() {
        return destinataries;
    }

    public void setDestinataries(EndInsSimpleList<User> destinataries) {
        this.destinataries = destinataries;
    }

}
