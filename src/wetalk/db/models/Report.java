package wetalk.db.models;

import java.io.Serializable;

public class Report implements Serializable {

    /**
     * Report attributes
     */
    private String id;
    private String title;
    private String clientMessage;
    private String adminResponse;
    private ReportStatus status;

    private User reportedUser;
    private User denouncerUser;

    /**
     * Creates a new report object.
     *
     * @param id report id
     * @param title report title
     * @param clientMessage description of the client
     * @param adminResponse admin resonse
     * @param status status of the report {@link ReportStatus}
     * @param denouncerUser user who made the report
     * @param reportedUser infractor user
     */
    public Report(String id, String title, String clientMessage,
            String adminResponse, ReportStatus status, User denouncerUser,
            User reportedUser) {
        this.id = id;
        this.title = title;
        this.clientMessage = clientMessage;
        this.adminResponse = adminResponse;
        this.status = status;
        this.denouncerUser = denouncerUser;
        this.reportedUser = reportedUser;
    }

    // Setters and getters for the class Report attributes
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClientMessage() {
        return clientMessage;
    }

    public void setClientMessage(String clientMessage) {
        this.clientMessage = clientMessage;
    }

    public String getAdminResponse() {
        return adminResponse;
    }

    public void setAdminResponse(String adminResponse) {
        this.adminResponse = adminResponse;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public User getReportedUser() {
        return reportedUser;
    }

    public void setReportedUser(User reportedUser) {
        this.reportedUser = reportedUser;
    }

    public User getDenouncerUser() {
        return denouncerUser;
    }

    public void setDenouncerUser(User denouncerUser) {
        this.denouncerUser = denouncerUser;
    }

    /**
     * Constants that represents the status of the report.
     */
    public enum ReportStatus {
        REJECTED,
        PENDING,
        ACCEPTED
    }

}
