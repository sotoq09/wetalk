package wetalk.db.models;

import java.io.Serializable;
import wetalk.lists.circular.EndInsCircularList;
import wetalk.lists.simple.StartInsSimpleList;

public class User implements Comparable<User>, Serializable {

    //User's attributes
    private String username;
    private String password;
    private String country;
    private String city;
    private int id;
    private String name;
    private String lastName;
    private String photoUri;
    private UserType type;

    // List of friends list
    private EndInsCircularList<FriendList> friendLists;

    // Messages
    private StartInsSimpleList<Message> sentMessages;
    private StartInsSimpleList<Message> receivedMessages;

    /**
     * Creates a new user object
     *
     * @param username Username
     * @param password Password
     * @param country Country
     * @param city City
     * @param id Identification
     * @param name Name
     * @param lastName Last name
     * @param type User type {@link UserTypes}
     */
    public User(String username, String password, String country, String city,
            int id, String name, String lastName, UserType type) {
        this.username = username;
        this.password = password;
        this.country = country;
        this.city = city;
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.type = type;

        this.friendLists = new EndInsCircularList<>();
        this.sentMessages = new StartInsSimpleList<>();
        this.receivedMessages = new StartInsSimpleList<>();
    }

    //User's attributes setters
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    //User's attributes getter 
    public String getUsername() {
        return username;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public String getPassword() {
        return password;
    }

    public UserType getType() {
        return type;
    }

    /**
     * Adds a new friend list to this user.
     *
     * @param friendList friend list to add
     * @return false if the list already exists
     */
    public boolean addFriendList(FriendList friendList) {
        if (friendLists.contains(friendList)) {
            return false;
        }

        friendLists.add(friendList);
        return true;
    }

    /**
     * Removes a list of friend from this user.
     *
     * @param friendList list to remove
     * @return false if the list doesn't exists
     */
    public boolean removeFriendList(FriendList friendList) {
        return friendLists.remove(friendList);
    }

    /**
     * Gets an specific friends list.
     *
     * @param listName name of the list to retrieve
     * @return the list or null if can't be found
     */
    public FriendList getFriendList(String listName) {
        return friendLists.searchElement(
                (FriendList t) -> t.getListName().equalsIgnoreCase(listName));
    }

    /**
     * @return all the lists of friends
     */
    public EndInsCircularList<FriendList> getAllFriendLists() {
        return friendLists;
    }

    /**
     * @return all sent messages
     */
    public StartInsSimpleList<Message> getSentMessages() {
        return sentMessages;
    }

    /**
     * @return all received messages
     */
    public StartInsSimpleList<Message> getReceivedMessages() {
        return receivedMessages;
    }

    /**
     * Compares this user with the specified user using the name.
     *
     * @param o the object to be compared
     * @return a negative integer, zero, or a positive integer as this object is
     * less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(User o) {
        return this.username.compareTo(o.username);
    }

    /**
     * Contants which represent what type of user is this.
     */
    public enum UserType {
        ADMIN,
        REGULAR
    }

}
