package wetalk.db.repository;

import wetalk.db.models.FriendList;
import wetalk.db.models.User;
import wetalk.lists.circular.EndInsCircularList;
import wetalk.lists.simple.EndInsSimpleList;

/**
 * Class used to manipulates all data related with the users
 */
public class FriendsRepository {

    private static FriendsRepository instance;

    /**
     * We are going to use the singleton pattern, so let's keep this private
     */
    private FriendsRepository() {
    }

    /**
     * @return the instance of the user repository
     */
    public static FriendsRepository getInstance() {
        if (instance == null) {
            instance = new FriendsRepository();
        }
        return instance;
    }

    /**
     * @param user the user to retrieve data
     * @return all the friend lists this user has
     */
    public EndInsCircularList<FriendList> getAllFriendLists(User user) {
        return user.getAllFriendLists();
    }

    /**
     * Return a friend list by it's name from the specified user.
     *
     * @param user the user to retrieve data
     * @param listName name of the list
     * @return the specified list if exists
     */
    public FriendList getFriendList(User user, String listName) {
        return user.getFriendList(listName);
    }

    /**
     * Add a new friend list to the specified user.
     *
     * @param user user
     * @param listName name of the list
     * @param listDescription description of the list
     * @return false if the list already exists
     */
    public boolean addFriendList(User user, String listName,
            String listDescription) {

        if (user.getFriendList(listName) != null) {
            return false;  // List already exists
        }

        user.addFriendList(new FriendList(listName, listDescription));
        return true;
    }

    /**
     * Modify the specified friend list.
     *
     * @param user user who has the friend list
     * @param friendList friend list to modify
     * @param newName new name
     * @param newDescription new description
     * @return false if a list with the new name already exists
     */
    public boolean modifyFriendList(User user, FriendList friendList,
            String newName, String newDescription) {
        if ((!friendList.getListName().equals(newName))
                && (user.getFriendList(newName) != null)) {
            return false;  // New name list already exists
        }

        friendList.setListName(newName);
        friendList.setListDescription(newDescription);
        return true;
    }

    /**
     * Removes a friend list from the specified user
     *
     * @param user user
     * @param listName name of the list
     * @return false if the list doesn't exists
     */
    public boolean removeFriendList(User user, String listName) {
        FriendList friendList = user.getFriendList(listName);

        if (friendList == null) {
            return false;  // List doesn't exists
        }

        return user.removeFriendList(friendList);
    }

    /**
     * Adds the specified friend to the specified list.
     *
     * @param friend friend to add
     * @param friendList friend list
     * @return false if the friend is already in the list
     */
    public boolean addFriend(User friend, FriendList friendList) {
        if (friendList.contains(friend)) {
            return false;
        }

        friendList.addFriend(friend);
        return true;
    }

    /**
     * Returns all the friend lists which contains the specified friend.
     *
     * @param user user with the lists
     * @param friend friend to check
     * @return a list with the results
     */
    public EndInsSimpleList<FriendList> searchFriendInLists(User user,
            User friend) {

        EndInsSimpleList<FriendList> results = new EndInsSimpleList<>();

        for (FriendList friendList : user.getAllFriendLists()) {
            if (friendList.contains(friend)) {
                results.add(friendList);
            }
        }

        return results;
    }

    /**
     * Removes a friend from the specified friend list.
     *
     * @param friendList the list
     * @param friend friend to remove
     * @return false if the friend isn't inside the list
     */
    public boolean removeFriend(FriendList friendList, User friend) {
        return friendList.removeFriend(friend);
    }

    public EndInsSimpleList<User> getCommonFriends(User user) {
        EndInsSimpleList<User> results = new EndInsSimpleList<>();

        EndInsSimpleList<User> userFriends = getAllUserFriends(user);

        for (User userToCheck : UserRepository.getInstance().getUserList()) {
            if (!userToCheck.equals(user)) {
                EndInsSimpleList<User> userToCheckFriends
                        = getAllUserFriends(userToCheck);

                for (User userToCheckFriend : userToCheckFriends) {
                    if (userFriends.contains(userToCheckFriend)) {
                        results.add(userToCheck);
                        break;
                    }
                }
            }
        }

        return results;
    }

    private EndInsSimpleList<User> getAllUserFriends(User user) {
        EndInsSimpleList<User> results = new EndInsSimpleList<>();

        for (FriendList friendList : user.getAllFriendLists()) {
            for (User friend : friendList.getFriends()) {
                results.add(friend);
            }
        }

        return results;
    }

}
