package wetalk.db.repository;

import wetalk.db.models.User;
import wetalk.lists.circular.DoubleCircularList;

/**
 * Class used to manipulates all data related with the users
 */
public class UserRepository {

    private static UserRepository instance;
    private DoubleCircularList<User> userList = new DoubleCircularList<>();

    /**
     * We are going to use the singleton pattern, so let's keep this private
     */
    private UserRepository() {
    }

    /**
     * @return the instance of the user repository
     */
    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    /**
     * Adds a new user.
     *
     * @param user user to add
     * @exception IllegalArgumentException if username is already in use or if
     * user id already exists
     */
    public void addUser(User user) {
        if (userNameExists(user.getUsername())) {
            throw new IllegalArgumentException("Username is already in use.");
        } else if (userIdExists(user.getId())) {
            throw new IllegalArgumentException("Id already exists.");
        }

        userList.add(user);
    }

    /**
     * Modifies the data of the specified user.
     *
     * @param user User to modify
     * @param imageUri new image path
     * @param username new username
     * @param name new name
     * @param lastName new lastname
     * @param city new city
     * @param country new country
     * @param id new id
     * @return false if an user with the same name already exists
     */
    public boolean modifyUser(User user, String imageUri, String username,
            String name, String lastName, String city, String country, int id) {
        if ((!user.getUsername().equalsIgnoreCase(username))
                && (userNameExists(username))) {
            return false;
        }
        if ((user.getId() != id) && (userIdExists(id))) {
            return false;
        }

        user.setPhotoUri(imageUri);
        user.setUsername(username);
        user.setName(name);
        user.setLastName(lastName);
        user.setCity(city);
        user.setCountry(country);
        user.setId(id);
        return true;
    }

    /**
     * Removes an user from the list.
     *
     * @param user user to remove
     * @return false if the user isn't in the list
     */
    public boolean removeUser(User user) {
        return userList.remove(user);
    }

    /**
     * Checks if an user with the specified user name already exists.
     *
     * @param username username to check
     * @return true if the username already exists
     */
    public boolean userNameExists(String username) {
        for (User user : userList) {
            if (user.getUsername().equalsIgnoreCase(username)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if an user with the specified id already exists.
     *
     * @param id id to check
     * @return true if the user id already exists
     */
    public boolean userIdExists(int id) {
        for (User user : userList) {
            if (user.getId() == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return the users list
     */
    public DoubleCircularList<User> getUserList() {
        return userList;
    }

    /**
     * Allows the data loader to load the user list on this repository. Internal
     * method, package visibility.
     *
     * @param list the list to load.
     */
    void setUserList(DoubleCircularList<User> list) {
        this.userList = list;
    }

}
