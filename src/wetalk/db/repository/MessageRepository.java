package wetalk.db.repository;

import java.util.UUID;
import wetalk.db.models.Message;
import wetalk.db.models.User;
import wetalk.lists.simple.EndInsSimpleList;
import wetalk.lists.simple.StartInsSimpleList;

/**
 * Class used to manipulates all data related with the messages
 */
public class MessageRepository {

    private static MessageRepository instance;

    /**
     * We are going to use the singleton pattern, so let's keep this private
     */
    private MessageRepository() {
    }

    /**
     * @return the instance of the user repository
     */
    public static MessageRepository getInstance() {
        if (instance == null) {
            instance = new MessageRepository();
        }
        return instance;
    }

    /**
     * Gets all the sent messages by the specified user.
     *
     * @param user user
     * @return all the sent messages from the user
     */
    public StartInsSimpleList<Message> getSentMessages(User user) {
        return user.getSentMessages();
    }

    /**
     * Gets all the received messages by the specified user.
     *
     * @param user user
     * @return all the sent received from the user
     */
    public StartInsSimpleList<Message> getReceivedMessages(User user) {
        return user.getReceivedMessages();
    }

    /**
     * Gets a sent message from the specified user using it's id.
     *
     * @param user user
     * @param messageId id of the message
     * @return the message if can be found
     */
    public Message getSentMessage(User user, String messageId) {
        return user.getSentMessages().searchElement((message) -> {
            return message.getId().equals(messageId);
        });
    }

    /**
     * Gets a received message from the specified user using it's id.
     *
     * @param user user
     * @param messageId id of the message
     * @return the message if can be found
     */
    public Message getReceivedMessage(User user, String messageId) {
        return user.getReceivedMessages().searchElement((message) -> {
            return message.getId().equals(messageId);
        });
    }

    /**
     * Removes a message sent by the specified user.
     *
     * @param user user
     * @param message message to delete
     * @return false if the user didn't sent the message
     */
    public boolean removeSentMessage(User user, Message message) {
        if (!user.getSentMessages().contains(message)) {
            return false;
        }

        // Removes the message from all the destinataries
        for (User destinatary : message.getDestinataries()) {
            destinatary.getReceivedMessages().remove(message);
        }

        user.getSentMessages().remove(message);
        return true;
    }

    /**
     * Modify a sent message by the specified user.
     *
     * @param user user
     * @param message message to modify
     * @param newText new text
     * @param newImageUri new image
     * @return true if the modification was successful
     */
    public boolean modifySentMessage(User user, Message message, String newText,
            String newImageUri) {
        message.setText(newText);
        message.setImageUri(newImageUri);
        return true;
    }

    /**
     * Sent a message.
     *
     * @param user the remitent
     * @param text text of the message
     * @param imageUri image of the message
     * @param destinataries list of destinataries
     */
    public void sentMessage(User user, String text, String imageUri,
            EndInsSimpleList<User> destinataries) {

        Message message = new Message(UUID.randomUUID().toString(), text,
                imageUri, user, destinataries);
        user.getSentMessages().add(message);

        for (User destinatary : destinataries) {
            destinatary.getReceivedMessages().add(message);
        }
    }

}
