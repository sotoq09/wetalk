package wetalk.db.repository;

import java.util.Date;
import java.util.UUID;
import wetalk.db.models.Event;
import wetalk.db.models.FriendList;
import wetalk.db.models.User;
import wetalk.lists.circular.DoubleCircularList;
import wetalk.lists.circular.EndInsCircularList;
import wetalk.lists.simple.EndInsSimpleList;

/**
 * Class used to manipulates all data related with the events
 */
public class EventRepository {

    private static EventRepository instance;
    private DoubleCircularList<Event> events;

    /**
     * We are going to use the singleton pattern, so let's keep this private
     */
    private EventRepository() {
        events = new DoubleCircularList<>();
    }

    /**
     * @return the instance of the user repository
     */
    public static EventRepository getInstance() {
        if (instance == null) {
            instance = new EventRepository();
        }
        return instance;
    }

    /**
     * Gets all the events that matches with the received status.
     *
     * @param status
     * @return all the events that matches the status
     */
    public EndInsSimpleList<Event> getEventsByFilter(
            Event.EventStatus status) {

        EndInsSimpleList<Event> results = new EndInsSimpleList<>();

        for (Event event : events) {
            if (event.getStatus() == status) {
                results.add(event);
            }
        }

        return results;
    }

    /**
     * Gets all the events created by the specified user and optionally filter
     * by status.
     *
     * @param user user that created the events
     * @param status status to filter can be null to return all
     * @return a list with all the events
     */
    public EndInsSimpleList<Event> getUserCreatedEvents(User user,
            Event.EventStatus status) {
        EndInsSimpleList<Event> results = new EndInsSimpleList<>();

        for (Event event : events) {
            if (event.getAuthor() == user) {
                if (status != null) {
                    if (event.getStatus() == status) {
                        results.add(event);
                    }
                } else {
                    results.add(event);
                }
            }
        }

        return results;
    }

    /**
     * Gets all the events in which this user is invited and optionally filter
     * by status.
     *
     * @param user user
     * @param status status to filter can be null to return all
     * @return a list with all the events
     */
    public EndInsSimpleList<Event> getUserInvitedEvents(User user,
            Event.EventStatus status) {
        EndInsSimpleList<Event> results = new EndInsSimpleList<>();

        for (Event event : events) {
            for (FriendList friendList : event.getFriendVisibility()) {
                if (friendList.contains(user)) {
                    if (status != null) {
                        if (event.getStatus() == status) {
                            results.add(event);
                        }
                    } else {
                        results.add(event);
                    }
                }
            }
        }

        return results;
    }

    /**
     * Creates a new event.
     *
     * @param name name of the event
     * @param description description of the event
     * @param city city of the event
     * @param date date of the event
     * @param status status {@link Event.EventStatus}
     * @param author author of the event
     * @param friendVisibility list with the friends that can see the event
     */
    public void createEvent(String name, String description, String city,
            Date date, Event.EventStatus status, User author,
            EndInsCircularList<FriendList> friendVisibility) {

        Event event = new Event(UUID.randomUUID().toString(), name, description,
                city, date, status, author, friendVisibility);
        events.add(event);
    }

    /**
     * Gets an event using it's id.
     *
     * @param id id of the event
     * @return the event
     */
    public Event getEvent(String id) {
        return events.searchElement((event) -> {
            return event.getId().equals(id);
        });
    }

    /**
     * Modifies an existing event.
     *
     * @param event event to modify
     * @param name new name
     * @param description new description
     * @param city new city
     * @param date new date
     * @param status new {@link Event.EventStatus}
     * @param friendVisibility new friendLists
     * @return false if the event already finished
     */
    public boolean modifyEvent(Event event, String name, String description,
            String city, Date date, Event.EventStatus status,
            EndInsCircularList<FriendList> friendVisibility) {

        if (status == Event.EventStatus.FINISHED) {
            return false;
        }

        event.setName(name);
        event.setDescription(description);
        event.setCity(city);
        event.setDate(date);
        event.setStatus(status);
        event.setFriendVisibility(friendVisibility);
        return true;
    }

    /**
     * Removes an event using it's id.
     *
     * @param event event to remove
     * @return false if the event doesn't exists
     */
    public boolean removeEvent(Event event) {
        return events.remove(event);
    }

    /**
     * @return the events list
     */
    public DoubleCircularList<Event> getEventsList() {
        return events;
    }

    /**
     * Allows the data loader to load the events list on this repository.
     * Internal method, package visibility.
     *
     * @param list the list to load.
     */
    void setEventsList(DoubleCircularList<Event> list) {
        this.events = list;
    }

}
