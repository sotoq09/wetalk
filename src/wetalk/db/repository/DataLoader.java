package wetalk.db.repository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import wetalk.db.models.Event;
import wetalk.db.models.Report;
import wetalk.db.models.User;
import wetalk.lists.circular.DoubleCircularList;
import wetalk.lists.circular.StartInsCircularList;

/**
 * This loads and saves all the programs data using files.
 */
public class DataLoader {
    
    public static void loadData() {
        ObjectInputStream objectInput = null;
        
        try (FileInputStream fileInput = new FileInputStream("wetalk.dat")) {
            objectInput = new ObjectInputStream(fileInput);
            
            DoubleCircularList<User> users = (DoubleCircularList) objectInput.readObject();
            UserRepository.getInstance().setUserList(users);
            
            StartInsCircularList<Report> reports = (StartInsCircularList) objectInput.readObject();
            ReportRepository.getInstance().setReportsList(reports);
            
            DoubleCircularList<Event> events = (DoubleCircularList) objectInput.readObject();
            EventRepository.getInstance().setEventsList(events);
            
            objectInput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (objectInput != null) {
                    objectInput.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void saveData() {
        ObjectOutputStream objectOutput = null;
        
        try (FileOutputStream fileOutput = new FileOutputStream("wetalk.dat")) {
            objectOutput = new ObjectOutputStream(fileOutput);
            objectOutput.writeObject(UserRepository.getInstance().getUserList());
            objectOutput.writeObject(ReportRepository.getInstance().getReportsList());
            objectOutput.writeObject(EventRepository.getInstance().getEventsList());
            objectOutput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (objectOutput != null) {
                    objectOutput.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
