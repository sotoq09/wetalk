package wetalk.db.repository;

import java.util.UUID;
import wetalk.db.models.Report;
import wetalk.db.models.User;
import wetalk.lists.circular.StartInsCircularList;
import wetalk.lists.simple.EndInsSimpleList;

/**
 * Class used to manipulates all data related with the reports
 */
public class ReportRepository {

    private static ReportRepository instance;
    private StartInsCircularList<Report> reports = new StartInsCircularList<>();

    /**
     * We are going to use the singleton pattern, so let's keep this private
     */
    private ReportRepository() {
    }

    /**
     * @return the instance of the user repository
     */
    public static ReportRepository getInstance() {
        if (instance == null) {
            instance = new ReportRepository();
        }
        return instance;
    }

    /**
     * Creates a new report
     *
     * @param title title
     * @param clientMessage message of the client
     * @param adminResponse response of the admin
     * @param status a {@link Report.ReportStatus}
     * @param denouncerUser the user who makes the denounce
     * @param reportedUser the denounced user
     */
    public void addReport(String title, String clientMessage,
            String adminResponse, Report.ReportStatus status,
            User denouncerUser, User reportedUser) {

        reports.add(new Report(UUID.randomUUID().toString(), title,
                clientMessage, adminResponse, status, denouncerUser,
                reportedUser));
    }

    /**
     * Gets all the reports matches the specified status.
     *
     * @param status the status to filter denounces
     * @return a list with all the denounces made by the user
     */
    public EndInsSimpleList<Report> getReports(Report.ReportStatus status) {
        EndInsSimpleList<Report> dataList = new EndInsSimpleList<>();

        for (Report report : reports) {
            if (report.getStatus() == status) {
                dataList.add(report);
            }
        }

        return dataList;
    }

    /**
     * Gets all the denounces that the specified user has made and matches the
     * specified status.
     *
     * @param denouncer the user
     * @param status the status to filter denounces
     * @return a list with all the denounces made by the user
     */
    public EndInsSimpleList<Report> getReportsByDenouncer(User denouncer,
            Report.ReportStatus status) {
        EndInsSimpleList<Report> dataList = new EndInsSimpleList<>();

        for (Report report : reports) {
            if (denouncer != null && report.getDenouncerUser() == denouncer
                    && report.getStatus() == status) {
                dataList.add(report);
            }
        }

        return dataList;
    }

    /**
     * Gets all the reports that the specified user has received and matches the
     * specified status.
     *
     * @param reported the user
     * @param status the status to filter denounces
     * @return a list with all the reports this user has received
     */
    public EndInsSimpleList<Report> getReportsByReported(User reported,
            Report.ReportStatus status) {
        EndInsSimpleList<Report> dataList = new EndInsSimpleList<>();

        for (Report report : reports) {
            if (reported != null && report.getReportedUser() == reported
                    && report.getStatus() == status) {
                dataList.add(report);
            }
        }

        return dataList;
    }

    /**
     * Get an specific report by it's id.
     *
     * @param reportId id of the report
     * @return the report, null if can't be found
     */
    public Report getReportById(String reportId) {
        return reports.searchElement((report) -> {
            return report.getId().equals(reportId);
        });
    }

    /**
     * Set the specified report status to ACCEPTED.
     *
     * @param report report
     * @param adminMsg message of the admin
     * @return false if the report status is already ACCEPTED or REJECTED
     */
    public boolean acceptReport(Report report, String adminMsg) {
        if (report.getStatus() != Report.ReportStatus.PENDING) {
            return false;
        }

        report.setAdminResponse(adminMsg);
        report.setStatus(Report.ReportStatus.ACCEPTED);
        return true;
    }

    /**
     * Set the specified report status to ACCEPTED.
     *
     * @param report report
     * @param adminMsg message of the admin
     * @return false if the report status is already ACCEPTED or REJECTED
     */
    public boolean rejectReport(Report report, String adminMsg) {
        if (report.getStatus() != Report.ReportStatus.PENDING) {
            return false;
        }

        report.setAdminResponse(adminMsg);
        report.setStatus(Report.ReportStatus.REJECTED);
        return true;
    }

    /**
     * @return the reports list
     */
    public StartInsCircularList<Report> getReportsList() {
        return reports;
    }

    /**
     * Allows the data loader to load the reports list on this repository.
     * Internal method, package visibility.
     *
     * @param list the list to load.
     */
    void setReportsList(StartInsCircularList<Report> list) {
        this.reports = list;
    }

}
