package wetalk;

import javax.swing.SwingUtilities;
import wetalk.db.repository.DataLoader;
import wetalk.ui.login.LoginWindow;

/**
 * Main entry point for the program
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Loads all the data
        DataLoader.loadData();

        // Creates and shows login window
        SwingUtilities.invokeLater(() -> {
            new LoginWindow().setVisible(true);
        });
    }

}
