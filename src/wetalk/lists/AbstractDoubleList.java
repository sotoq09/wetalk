package wetalk.lists;

import java.io.Serializable;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * Provides an skeleton for a simple list.
 *
 * @param <E> the type of elements in this list
 */
public abstract class AbstractDoubleList<E> implements List<E>,
        Serializable {

    protected int size;
    public Node<E> firstNode = null;

    /**
     * Constructs an empty list
     */
    public AbstractDoubleList() {
        this.size = 0;
    }

    /**
     * Returns the element at the specified position.
     *
     * @param index index
     * @return the element at the specified position
     */
    @Override
    public E get(int index) {
        // Check if the first is null;
        if (firstNode == null) {
            return null;
        }

        Node<E> temp = firstNode;

        int i = 0;
        do {
            if (i == index) {
                return temp.item;
            }
            temp = temp.next;
            i++;
        } while ((temp != null) && (temp != firstNode));

        return null;
    }

    /**
     * Returns true if this list contains the specified element.
     *
     * @param element the element to check if exists in this list
     * @return true if this list contains the specified element
     */
    @Override
    public boolean contains(E element) {
        // Check if the first is null;
        if (firstNode == null) {
            return false;
        }

        Node<E> temp = firstNode;

        do {
            if (temp.item.equals(element)) {
                return true;
            }
            temp = temp.next;
        } while ((temp != null) && (temp != firstNode));

        return false;
    }

    /**
     * @return true if there is no elements in this list
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Removes the first item that it's equals to the specified element.
     *
     * @param element the element to be removed
     * @return true if the element was deleted
     */
    @Override
    public boolean remove(E element) {
        return removeIf((E t) -> t.equals(element));
    }

    /**
     * Removes the first element in this list that satisfy the specified
     * condition.
     *
     * @param filter a predicate with the conditions
     * @return true if any element was removed
     */
    @Override
    public boolean removeIf(Predicate<E> filter) {
        if (firstNode == null) {
            return false;
        }

        Node<E> temp = firstNode;

        do {
            if (filter.test(temp.item)) {
                if (temp.prev == null) {  // If has no elements behind
                    temp.next.prev = null;
                    firstNode = temp.next;
                } else if (temp.next == null) {  // No elements in front
                    temp.prev.next = null;
                } else {  // Has elements behind and in front
                    temp.prev.next = temp.next;
                    temp.next.prev = temp.prev;

                    if (temp == firstNode) {
                        // We need to check the first element in case is
                        // a circular list
                        firstNode = (firstNode != firstNode.next)
                                ? firstNode.next : null;
                    }
                }
                size--;
                return true; // Successful delete
            }

            temp = temp.next;
        } while ((temp != null) && (temp != firstNode));

        return false;
    }

    /**
     * Return the first element in this list that satify the specified
     * condition.
     *
     * @param filter a predicate with the conditions
     * @return the first element that matches the conditions if there is any
     */
    @Override
    public E searchElement(Predicate<E> filter) {
        // Check if the first is null;
        if (firstNode == null) {
            return null;
        }

        Node<E> temp = firstNode;

        do {
            if (filter.test(temp.item)) {
                return temp.item;
            }
            temp = temp.next;
        } while ((temp != null) && (temp != firstNode));

        return null;
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * @return an iterator for this list
     */
    @Override
    public Iterator<E> iterator() {
        return new ListIterator();
    }

    /**
     * Implementation of an iterator for this list.
     */
    protected class ListIterator implements Iterator<E> {

        private Node<E> index = firstNode;
        private boolean firstTime = true;

        @Override
        public boolean hasNext() {
            if (firstTime) {
                firstTime = false;
                return index != null;
            }
            return index != null && index != firstNode;
        }

        @Override
        public E next() {
            E item = index.item;
            index = index.next;
            return item;
        }

    }

    protected static class Node<E> implements Serializable {

        public E item; // Item of the node
        public Node<E> next; // Memory direction of the next node
        public Node<E> prev; //Memory direction of the previous node

        public Node(E item, Node<E> next, Node<E> prev) {
            this.item = item;
            this.next = next;
            this.prev = prev;
        }
    }

}
