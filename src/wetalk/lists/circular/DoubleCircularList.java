package wetalk.lists.circular;

import wetalk.lists.AbstractDoubleList;

/**
 * An implementation of double circular list with sorted insertion. The elements
 * must implement {@link Comparable}
 *
 * @param <E> the type of elements in this list
 */
public class DoubleCircularList<E extends Comparable>
        extends AbstractDoubleList<E> {

    /**
     * Adds the specified element and keeps the list sorted. Element can't be
     * null
     *
     * @param element element to be added to this list
     * @exception IllegalArgumentException if the element is null
     */
    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element, null, null);
        size++;
        
        // If there is no elements
        if (this.firstNode == null) {
            this.firstNode = newNode;

            // Double circular link
            this.firstNode.next = newNode;
            this.firstNode.prev = newNode;
        } else {
            Node<E> temp = firstNode;

            // Iterate through the list
            do {
                // If the element to add is less than the current list element
                if (element.compareTo(temp.item) <= 0) {
                    // Link the new element with the current and 
                    // the current previous
                    newNode.next = temp;
                    newNode.prev = temp.prev;

                    // Link the previous next with the new
                    temp.prev.next = newNode;
                    temp.prev = newNode;

                    // If the current element is the first, update the first
                    if (temp == firstNode) {
                        firstNode = newNode;
                    }
                    return; // End of the method
                }
                temp = temp.next;
            } while (temp.next != firstNode);

            // If we made it this far is because the element has to be added
            // at the end
            temp.next = newNode;
            newNode.prev = temp;
            newNode.next = firstNode;
            firstNode.prev = newNode;
        }
    }

}
