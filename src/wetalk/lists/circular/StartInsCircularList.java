/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wetalk.lists.circular;

import wetalk.lists.AbstractSimpleList;

/**
 * An implementation of a simple circular list with insertion at the end.
 *
 * @param <E> the type of elements in this list
 */
public class StartInsCircularList<E> extends AbstractSimpleList<E> {

    /**
     * Adds the specified element at the end. Element can't be null
     *
     * @param element element to be added to this list
     * @exception IllegalArgumentException if the element is null
     */
    @Override
    public void add(E element) {
        AbstractSimpleList.Node<E> newNode = new AbstractSimpleList.Node<>(element, null);
        size++;

        // If there is no elements
        if (this.firstNode == null) {
            this.firstNode = newNode;

            // Circular link
            this.firstNode.next = newNode;
        } else {
            AbstractSimpleList.Node<E> temp = firstNode;

            do {
                temp = temp.next;
            } while (temp.next != firstNode);

            temp.next = newNode;
            newNode.next = firstNode;
            firstNode = newNode;
        }
    }

}
