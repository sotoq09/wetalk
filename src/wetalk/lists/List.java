package wetalk.lists;

import java.util.function.Predicate;

/**
 * Defines the basic list methods
 *
 * @param <E> the type of elements in this list
 */
public interface List<E> extends Iterable<E> {

    /**
     * Adds the specified element to this list.
     *
     * @param element element to be added to this list
     */
    void add(E element);

    /**
     * Returns the element at the specified position.
     *
     * @param index index
     * @return the element at the specified position
     */
    E get(int index);

    /**
     * Returns true if this list contains the specified element.
     *
     * @param element the element to check if exists in this list
     * @return true if this list contains the specified element
     */
    boolean contains(E element);

    /**
     * @return true if there is no elements in this list
     */
    boolean isEmpty();

    /**
     * Removes the first item that it's equals to the specified element.
     *
     * @param element the element to be removed
     * @return true if the element was deleted
     */
    boolean remove(E element);

    /**
     * Removes the first element in this list that satisfy the specified
     * condition.
     *
     * @param filter a predicate with the conditions
     * @return true if any element was removed
     */
    boolean removeIf(Predicate<E> filter);

    /**
     * Return the first element in this list that satify the specified
     * condition.
     *
     * @param filter a predicate with the conditions
     * @return the first element that matches the conditions if there is any
     */
    E searchElement(Predicate<E> filter);

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    int size();

}
