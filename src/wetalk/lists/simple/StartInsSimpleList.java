package wetalk.lists.simple;

import wetalk.lists.AbstractSimpleList;

/**
 * An implementation of simple list with insertion at the begining.
 *
 * @param <E> the type of elements in this list
 */
public class StartInsSimpleList<E> extends AbstractSimpleList<E> {

    /**
     * Adds the specified element at the start of this list. Element can't be
     * null
     *
     * @param element element to be added to this list
     * @exception IllegalArgumentException if the element is null
     */
    @Override
    public void add(E element) {
        if (element == null) {
            throw new IllegalArgumentException("List element can't be null.");
        }

        // If the list is empty
        if (firstNode == null) {
            firstNode = new AbstractSimpleList.Node<>(element, null);
        } else {
            Node<E> newNode = new AbstractSimpleList.Node<>(element, null);
            newNode.next = firstNode;
            firstNode = newNode;
        }

        size++; // Increase the size of the list
    }

}
