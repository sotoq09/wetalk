package wetalk.lists.simple;

import wetalk.lists.AbstractSimpleList;

/**
 * An implementation of simple list with insertion at the end.
 *
 * @param <E> the type of elements in this list
 */
public class EndInsSimpleList<E> extends AbstractSimpleList<E> {

    /**
     * Adds the specified element at the end of this list. Element can't be null
     *
     * @param element element to be added to this list
     * @exception IllegalArgumentException if the element is null
     */
    @Override
    public void add(E element) {
        if (element == null) {
            throw new IllegalArgumentException("List element can't be null.");
        }

        // If the list is empty
        if (firstNode == null) {
            firstNode = new Node<>(element, null);
        } else {
            Node<E> temp = firstNode;

            // Get to the end of the list to insert the element
            while (temp.next != null) {
                temp = temp.next;
            }

            // Insert the element at the end
            temp.next = new Node<>(element, null);
        }

        size++; // Increase the size of the list
    }

}
